package com.desafio.microservice.v1.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

import com.desafio.microservice.exception.FindException;
import com.desafio.microservice.exception.RegisterException;
import com.desafio.microservice.v1.entity.Product;
import com.desafio.microservice.v1.model.ProductVO;
import com.desafio.microservice.v1.repository.ProductRepository;

import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("unit-test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductServiceUnitTest {

	@InjectMocks
	private ProductService productService;

	@Mock
	private ProductRepository repository;

	private Optional<Product> product;
	private List<Product> listProducts;

	private static final String MSG_PRODUCT_NOT_FOUND = "Produto não encontrado na base";
	private static final String MSG_REGISTER_INVALID_VALUES = "Cadastro não aceita valores em branco ou vazio";
	private static final String MSG_UPDATE_INVALID_VALUES = "A atualização de produtos não aceita valores em branco ou vazio";
	private static final String MSG_PRICE_NEGATIVE = "Campo 'PRICE' não aceita valores negativos";

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
		this.product = initProduct();
		this.listProducts = initListProducts();
	}

	@Test
	public void testGetAllSuccess() {
		Mockito.when(repository.findByAll()).thenReturn(listProducts);

		this.productService.getAllProducts().map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();

	}

	@Test
	public void testGetAllByParamsSuccess() {
		Mockito.when(repository.findByNameOrDescriptionAndPriceBetween("Biscoito", "Biscoito", 1.0, 100.0))
				.thenReturn(listProducts);

		this.productService.getAllProductsByParams(1.0, 100.0, "Biscoito").map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();

	}
	
	@Test
	public void testGetAllByParamsSuccess2() {
		Mockito.when(repository.findByNameOrDescription("Biscoito", "Biscoito"))
				.thenReturn(listProducts);

		this.productService.getAllProductsByParams(null, null, "Biscoito").map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();

	}
	
	@Test
	public void testGetAllByParamsSuccess3() {
		Mockito.when(repository.findByPriceBetween( 1.0, 100.0))
				.thenReturn(listProducts);

		this.productService.getAllProductsByParams(1.0, 100.0, "").map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();

	}
	
	@Test
	public void testGetAllByParamsSuccess4() {
		Mockito.when(repository.findByNameOrDescriptionAndPriceGreaterThanEqual("Biscoito", "Biscoito", 1.0))
				.thenReturn(listProducts);

		this.productService.getAllProductsByParams(1.0, null, "Biscoito").map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();

	}
	
	@Test
	public void testGetAllByParamsSuccess5() {
		Mockito.when(repository.findByNameOrDescriptionAndPriceLessThanEqual("Biscoito", "Biscoito", 100.0))
				.thenReturn(listProducts);

		this.productService.getAllProductsByParams(null, 100.0, "Biscoito").map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();

	}
	
	@Test
	public void testGetAllByParamsSuccess6() {
		Mockito.when(repository.findByPriceGreaterThanEqual(1.0))
				.thenReturn(listProducts);

		this.productService.getAllProductsByParams(1.0, null, "").map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();

	}
	
	@Test
	public void testGetAllByParamsSuccess7() {
		Mockito.when(repository.findByPriceLessThanEqual(100.0))
				.thenReturn(listProducts);

		this.productService.getAllProductsByParams(null, 100.0, "").map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();

	}
	
	@Test
	public void testGetAllByParamsSuccess8() {
		Mockito.when(repository.findByPriceLessThanEqual(100.0))
				.thenReturn(listProducts);

		this.productService.getAllProductsByParams(null, 100.0, null).map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();

	}
	
	@Test
	public void testGetAllByParamsSuccess9() {

		this.productService.getAllProductsByParams(null, null, null).map(result -> {
			Assert.assertEquals(new ArrayList<>(), result);
			return result;
		}).subscribe();

	}

	@Test
	public void testGetByIdSuccess() {
		Mockito.when(repository.findById("1")).thenReturn(product);

		this.productService.getProductById("1").map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();
	}

	@Test
	public void testGetByIdException() {

		this.productService.getProductById("1").onErrorResume(error -> {
			FindException erro = (FindException) error;
			Assert.assertEquals(HttpStatus.NOT_FOUND, erro.getStatus());
			Assert.assertEquals(MSG_PRODUCT_NOT_FOUND, erro.getMessage());
			return Mono.empty();
		}).subscribe();
	}

	@Test
	public void testDeleteProductSuccess() {
		Mockito.when(repository.findById("1")).thenReturn(product);
		this.productService.deleteProduct("1").map(result -> {
			Assert.assertNotNull(result);
			return result;
		}).subscribe();

	}

	@Test
	public void testDeleteProductException() {
		this.productService.deleteProduct("1").doOnError(erro -> {
			Assert.assertEquals(MSG_PRODUCT_NOT_FOUND, erro.getMessage());
		}).subscribe();
	}

	@Test
	public void testRegisterProductSuccess() {
		Product productT = new Product("1", "Bolacha", "Agua e Sal", 5.0);
		Mockito.when(repository.save(new Product("Bolacha", "Agua e Sal", 5.0))).thenReturn(productT);
		ProductVO productVO = new ProductVO("Bolacha", "Agua e Sal", 5.0);

		this.productService.addProduct(productVO).doOnSuccess((result -> {
			Assert.assertEquals(productT, result);
			return;
		})).subscribe();

	}

	@Test
	public void testUpdateProductSuccess() {
		Product productT = new Product("1", "Bolacha", "Agua e Sal", 5.0);
		Mockito.when(repository.findById("1")).thenReturn(product);
		Mockito.when(repository.save(productT)).thenReturn(productT);
		ProductVO productVO = new ProductVO("Bolacha", "Agua e Sal", 5.0);

		this.productService.updateProduct("1", productVO).doOnSuccess((result -> {
			Assert.assertEquals(productT, result);
			return;
		})).subscribe();

	}

	@Test
	public void testRegisterProductException() {

		ProductVO productVO = new ProductVO("Bolacha", "", 5.0);

		this.productService.addProduct(productVO).onErrorResume(error -> {
			RegisterException erro = (RegisterException) error;
			Assert.assertEquals(HttpStatus.BAD_REQUEST, erro.getStatus());
			Assert.assertEquals(MSG_REGISTER_INVALID_VALUES, erro.getMessage());
			return Mono.empty();
		}).subscribe();

	}

	@Test
	public void testRegisterProductException2() {

		ProductVO productVO = new ProductVO("Bolacha", "Agua e Sal", -5.0);

		this.productService.addProduct(productVO).onErrorResume(error -> {
			RegisterException erro = (RegisterException) error;
			Assert.assertEquals(HttpStatus.BAD_REQUEST, erro.getStatus());
			Assert.assertEquals(MSG_PRICE_NEGATIVE, erro.getMessage());
			return Mono.empty();
		}).subscribe();
	}

	@Test
	public void testRegisterProductException3() {

		ProductVO productVO = new ProductVO("", "Bolacha", 5.0);

		this.productService.addProduct(productVO).onErrorResume(error -> {
			RegisterException erro = (RegisterException) error;
			Assert.assertEquals(HttpStatus.BAD_REQUEST, erro.getStatus());
			Assert.assertEquals(MSG_REGISTER_INVALID_VALUES, erro.getMessage());
			return Mono.empty();
		}).subscribe();

	}

	@Test
	public void testRegisterProductException4() {

		ProductVO productVO = new ProductVO("Bolacha", "Bolacha", null);

		this.productService.addProduct(productVO).onErrorResume(error -> {
			RegisterException erro = (RegisterException) error;
			Assert.assertEquals(HttpStatus.BAD_REQUEST, erro.getStatus());
			Assert.assertEquals(MSG_REGISTER_INVALID_VALUES, erro.getMessage());
			return Mono.empty();
		}).subscribe();

	}

	@Test
	public void testUpdateProductException() {

		ProductVO productVO = new ProductVO("Bolacha", "", 5.0);
		Mockito.when(repository.findById("1")).thenReturn(product);

		this.productService.updateProduct("1", productVO).onErrorResume(error -> {
			RegisterException erro = (RegisterException) error;
			Assert.assertEquals(HttpStatus.BAD_REQUEST, erro.getStatus());
			Assert.assertEquals(MSG_UPDATE_INVALID_VALUES, erro.getMessage());
			return Mono.empty();
		}).subscribe();

	}

	@Test
	public void testUpdateProductException2() {

		ProductVO productVO = new ProductVO("Bolacha", "Agua e Sal", -5.0);
		Mockito.when(repository.findById("1")).thenReturn(product);

		this.productService.updateProduct("1", productVO).onErrorResume(error -> {
			RegisterException erro = (RegisterException) error;
			Assert.assertEquals(HttpStatus.BAD_REQUEST, erro.getStatus());
			Assert.assertEquals(MSG_PRICE_NEGATIVE, erro.getMessage());
			return Mono.empty();
		}).subscribe();

	}

	@Test
	public void testUpdateProductException3() {

		ProductVO productVO = new ProductVO("Bolacha", "", 5.0);
		Mockito.when(repository.findById("1")).thenReturn(Optional.empty());

		this.productService.updateProduct("1", productVO).onErrorResume(error -> {
			RegisterException erro = (RegisterException) error;
			Assert.assertEquals(HttpStatus.NOT_FOUND, erro.getStatus());
			Assert.assertEquals(MSG_PRODUCT_NOT_FOUND, erro.getMessage());
			return Mono.empty();
		}).subscribe();

	}

	@Test
	public void testUpdateProductException4() {

		ProductVO productVO = new ProductVO("", "Bolacha", 5.0);
		Mockito.when(repository.findById("1")).thenReturn(product);

		this.productService.updateProduct("1", productVO).onErrorResume(error -> {
			RegisterException erro = (RegisterException) error;
			Assert.assertEquals(HttpStatus.BAD_REQUEST, erro.getStatus());
			Assert.assertEquals(MSG_UPDATE_INVALID_VALUES, erro.getMessage());
			return Mono.empty();
		}).subscribe();

	}

	@Test
	public void testUpdateProductException5() {

		ProductVO productVO = new ProductVO("Bolacha", "Bolacha", null);
		Mockito.when(repository.findById("1")).thenReturn(product);

		this.productService.updateProduct("1", productVO).onErrorResume(error -> {
			RegisterException erro = (RegisterException) error;
			Assert.assertEquals(HttpStatus.BAD_REQUEST, erro.getStatus());
			Assert.assertEquals(MSG_UPDATE_INVALID_VALUES, erro.getMessage());
			return Mono.empty();
		}).subscribe();

	}

	private Optional<Product> initProduct() {
		Product productT = new Product("1", "Bono Chocolate", "Biscoito", 2.5);
		Optional<Product> product = Optional.ofNullable(productT);
		return product;
	}

	private List<Product> initListProducts() {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("1", "Bono Chocolate", "Biscoito", 2.5));
		products.add(new Product("2", "Bono Morango", "Biscoito", 3.0));
		products.add(new Product("3", "Bono Doce de Leite", "Biscoito", 4.0));
		return products;
	}

}
