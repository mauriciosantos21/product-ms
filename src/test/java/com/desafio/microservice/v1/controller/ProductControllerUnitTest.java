package com.desafio.microservice.v1.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import com.desafio.microservice.exception.FindException;
import com.desafio.microservice.exception.RegisterException;
import com.desafio.microservice.v1.entity.Product;
import com.desafio.microservice.v1.model.ProductVO;
import com.desafio.microservice.v1.model.ResponseError;
import com.desafio.microservice.v1.service.IProductService;

import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("controller-test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductControllerUnitTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@MockBean
	private IProductService productService;

	private Optional<Product> product;
	private List<Product> listProducts;

	private static final String MSG_PRODUCT_NOT_FOUND = "Produto não encontrado na base";
	private static final String MSG_REGISTER_INVALID_VALUES = "Cadastro não aceita valores em branco ou vazio";
	private static final String MSG_UPDATE_INVALID_VALUES = "A atualização de produtos não aceita valores em branco ou vazio";
	private static final String MSG_PRICE_NEGATIVE = "Campo 'PRICE' não aceita valores negativos";

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
		this.product = initProduct();
		this.listProducts = initListProducts();
	}

	@Test
	public void testGetByIdSuccess() {

		Mockito.when(productService.getProductById("1")).thenReturn(Mono.just(product));

		ResponseEntity<Optional<Product>> response = restTemplate.exchange("http://localhost:" + port + "/products/1",
				HttpMethod.GET, null, new ParameterizedTypeReference<Optional<Product>>() {
				});

		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assert.assertEquals(this.product.get(), response.getBody().get());

	}

	@Test
	public void testGetByIdException() {

		Mockito.when(productService.getProductById("1"))
				.thenThrow(new FindException(MSG_PRODUCT_NOT_FOUND, HttpStatus.NOT_FOUND));

		ResponseEntity<Optional<Product>> response = restTemplate.exchange("http://localhost:" + port + "/products/1",
				HttpMethod.GET, null, new ParameterizedTypeReference<Optional<Product>>() {
				});

		Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

	}

	@Test
	public void testGetProductsSuccess() {

		Mockito.when(productService.getAllProducts()).thenReturn(Mono.just(listProducts));
		ResponseEntity<Iterable<Product>> response = restTemplate.exchange("http://localhost:" + port + "/products",
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<Product>>() {
				});

		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assert.assertEquals(listProducts, response.getBody());
	}

	@Test
	public void testGetProductsEmpty() {
		Mockito.when(productService.getAllProducts()).thenReturn((Mono.just(new ArrayList<Product>())));

		ResponseEntity<Iterable<Product>> response = restTemplate.exchange("http://localhost:" + port + "/products",
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<Product>>() {
				});

		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assert.assertEquals(new ArrayList<Product>(), response.getBody());
	}

	@Test
	public void testGetProductsByParamsSuccess() {

		Mockito.when(productService.getAllProductsByParams(1.0, 50.0, "Biscoito")).thenReturn(Mono.just(new ArrayList<Product>()));
		ResponseEntity<Iterable<Product>> response = restTemplate.exchange(
				"http://localhost:" + port + "/products/search?min_price=1.0&max_price=50.0&q=Biscoito", HttpMethod.GET,
				null, new ParameterizedTypeReference<Iterable<Product>>() {
				});

		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assert.assertEquals(new ArrayList<Product>(), response.getBody());
	}

	@Test
	public void testGetProductsByParamsEmptyList() {

		Mockito.when(productService.getAllProductsByParams(1.0, 50.0, "Biscoito")).thenReturn(Mono.just(listProducts));
		ResponseEntity<Iterable<Product>> response = restTemplate.exchange(
				"http://localhost:" + port + "/products/search?min_price=1.0&max_price=50.0&q=Biscoito", HttpMethod.GET,
				null, new ParameterizedTypeReference<Iterable<Product>>() {
				});

		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assert.assertEquals(listProducts, response.getBody());
	}

	@Test
	public void testRegisterProductSuccess() {
		Product productT = new Product("1", "Bolacha", "Agua e Sal", 5.0);
		ProductVO productVO = new ProductVO("Bolacha", "Agua e Sal", 5.0);

		Mockito.when(productService.addProduct(productVO)).thenReturn(Mono.just(productT));
		HttpEntity<ProductVO> requestEntity = new HttpEntity<>(productVO);
		ResponseEntity<Product> response = restTemplate.exchange("http://localhost:" + port + "/products",
				HttpMethod.POST, requestEntity, new ParameterizedTypeReference<Product>() {
				});

		Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
		Assert.assertEquals(productT, response.getBody());
	}

	@Test
	public void testRegisterProductRegisterException() {
		ProductVO productVO = new ProductVO("Bolacha", "", 5.0);

		Mockito.when(productService.addProduct(productVO))
				.thenThrow(new RegisterException(MSG_REGISTER_INVALID_VALUES, HttpStatus.BAD_REQUEST));
		HttpEntity<ProductVO> requestEntity = new HttpEntity<>(productVO);
		ResponseEntity<ResponseError> response = restTemplate.exchange("http://localhost:" + port + "/products",
				HttpMethod.POST, requestEntity, new ParameterizedTypeReference<ResponseError>() {
				});

		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		Assert.assertEquals(MSG_REGISTER_INVALID_VALUES, response.getBody().getMessage());
	}

	@Test
	public void testRegisterProductRegisterException2() {
		ProductVO productVO = new ProductVO("Bolacha", "Agua e Sal", -1.0);

		Mockito.when(productService.addProduct(productVO))
				.thenThrow(new RegisterException(MSG_PRICE_NEGATIVE, HttpStatus.BAD_REQUEST));
		HttpEntity<ProductVO> requestEntity = new HttpEntity<>(productVO);
		ResponseEntity<ResponseError> response = restTemplate.exchange("http://localhost:" + port + "/products",
				HttpMethod.POST, requestEntity, new ParameterizedTypeReference<ResponseError>() {
				});

		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		Assert.assertEquals(MSG_PRICE_NEGATIVE, response.getBody().getMessage());
	}

	@Test
	public void testUpdateProductSuccess() {
		Product productT = new Product("1", "Bolacha", "Agua e Sal", 5.0);
		ProductVO productVO = new ProductVO("Biscoito", "Agua e Sal", 5.0);

		Mockito.when(productService.updateProduct("1", productVO)).thenReturn(Mono.just(productT));
		HttpEntity<ProductVO> requestEntity = new HttpEntity<>(productVO);
		ResponseEntity<Product> response = restTemplate.exchange("http://localhost:" + port + "/products/1",
				HttpMethod.PUT, requestEntity, new ParameterizedTypeReference<Product>() {
				});

		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assert.assertEquals(productT, response.getBody());
	}

	@Test
	public void testUpdateProductException1() {
		ProductVO productVO = new ProductVO("Bolacha", "", 5.0);

		Mockito.when(productService.updateProduct("1", productVO))
				.thenThrow(new RegisterException(MSG_UPDATE_INVALID_VALUES, HttpStatus.BAD_REQUEST));
		HttpEntity<ProductVO> requestEntity = new HttpEntity<>(productVO);
		ResponseEntity<ResponseError> response = restTemplate.exchange("http://localhost:" + port + "/products/1",
				HttpMethod.PUT, requestEntity, new ParameterizedTypeReference<ResponseError>() {
				});

		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		Assert.assertEquals(MSG_UPDATE_INVALID_VALUES, response.getBody().getMessage());
	}

	@Test
	public void testUpdateProductException2() {
		ProductVO productVO = new ProductVO("Bolacha", "Agua e Sal", -1.0);

		Mockito.when(productService.updateProduct("1", productVO))
				.thenThrow(new RegisterException(MSG_PRICE_NEGATIVE, HttpStatus.BAD_REQUEST));
		HttpEntity<ProductVO> requestEntity = new HttpEntity<>(productVO);
		ResponseEntity<ResponseError> response = restTemplate.exchange("http://localhost:" + port + "/products/1",
				HttpMethod.PUT, requestEntity, new ParameterizedTypeReference<ResponseError>() {
				});

		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		Assert.assertEquals(MSG_PRICE_NEGATIVE, response.getBody().getMessage());
	}

	@Test
	public void testDeleteProductSuccess() {
		Mockito.when(productService.deleteProduct("1")).thenReturn(Mono.just(true));
		ResponseEntity<Boolean> response = restTemplate.exchange("http://localhost:" + port + "/products/1",
				HttpMethod.DELETE, null, new ParameterizedTypeReference<Boolean>() {
				});

		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void testDeleteProductException() {
		Mockito.when(productService.deleteProduct("1"))
				.thenThrow(new FindException(MSG_PRODUCT_NOT_FOUND, HttpStatus.NOT_FOUND));
		ResponseEntity<Boolean> response = restTemplate.exchange("http://localhost:" + port + "/products/1",
				HttpMethod.DELETE, null, new ParameterizedTypeReference<Boolean>() {
				});

		Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	private Optional<Product> initProduct() {
		Product productT = new Product("1", "Bono Chocolate", "Biscoito", 2.5);
		Optional<Product> product = Optional.ofNullable(productT);
		return product;
	}

	private List<Product> initListProducts() {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("1", "Bono Chocolate", "Biscoito", 2.5));
		products.add(new Product("2", "Bono Morango", "Biscoito", 3.0));
		products.add(new Product("3", "Bono Doce de Leite", "Biscoito", 4.0));
		return products;
	}

}
