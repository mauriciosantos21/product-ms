package com.desafio.microservice.exception;

import org.springframework.http.HttpStatus;

public class RegisterException extends MsException{
	
	private static final long serialVersionUID = 3754617852771593219L;
	
	public RegisterException(String mensagem, HttpStatus status) {
		super(mensagem, status);
	}
	
}
