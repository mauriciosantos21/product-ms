package com.desafio.microservice.exception;

import org.springframework.http.HttpStatus;

public class FindException extends MsException{

	private static final long serialVersionUID = -6152252284654307521L;
	
	public FindException(String mensagem, HttpStatus status) {
		super(mensagem, status);
	}

}
