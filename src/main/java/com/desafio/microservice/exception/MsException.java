package com.desafio.microservice.exception;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class MsException extends RuntimeException implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5740333759195888003L;
	
	private final HttpStatus status;
	
	public MsException(String mensagem, HttpStatus status) {
		super(mensagem);
		this.status = status;
	}
	
	public MsException(String mensagem, HttpStatus status, Throwable cause) {
		super(mensagem,cause);
		this.status = status;
	}

}
