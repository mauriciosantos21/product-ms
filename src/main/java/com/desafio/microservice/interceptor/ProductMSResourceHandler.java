package com.desafio.microservice.interceptor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.desafio.microservice.exception.FindException;
import com.desafio.microservice.exception.RegisterException;
import com.desafio.microservice.v1.model.ResponseError;

@ControllerAdvice
public class ProductMSResourceHandler{
	
	@ExceptionHandler(RegisterException.class)
	public ResponseEntity<ResponseError> handlerReponseRegisterException(RegisterException r) {
		ResponseError responseError = new ResponseError(r.getStatus().value(), r.getMessage());
		
		return ResponseEntity.status(r.getStatus()).body(responseError);
	}
	
	@ExceptionHandler(FindException.class)
	public ResponseEntity<ResponseError> handlerResponseFindException(FindException r){
		return ResponseEntity.status(r.getStatus()).body(null);
	}

}
