package com.desafio.microservice.v1.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.microservice.v1.entity.Product;
import com.desafio.microservice.v1.model.ProductVO;
import com.desafio.microservice.v1.service.IProductService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("")
@CrossOrigin(origins = "*")
public class ProductController {

	@Autowired
	private IProductService productService;

	@GetMapping("/products")
	public ResponseEntity<Mono<List<Product>>> listAllProducts() {
		return ResponseEntity.status(HttpStatus.OK).body(this.productService.getAllProducts());
	}

	@GetMapping("/products/{id}")
	public ResponseEntity<Mono<Optional<Product>>> getProductById(@PathVariable String id) {
		return ResponseEntity.status(HttpStatus.OK).body(this.productService.getProductById(id));
	}

	@GetMapping("/products/search")
	public ResponseEntity<Mono<List<Product>>> getAllProductByParams(@RequestParam(name = "min_price", required=false) Double minPrice,
			@RequestParam(name = "max_price",required=false) Double maxPrice, @RequestParam(required=false) String q) {
		return ResponseEntity.status(HttpStatus.OK).body(this.productService.getAllProductsByParams(minPrice, maxPrice, q));
	}
	
	@PostMapping("/products")
	public ResponseEntity<Mono<Product>> addProduct(@RequestBody ProductVO productVO){		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.productService.addProduct(productVO));		
	}
	
	@PutMapping("/products/{id}")
	public ResponseEntity<Mono<Product>> updateProduct(@PathVariable String id, @RequestBody ProductVO productVO){
		return ResponseEntity.status(HttpStatus.OK).body(this.productService.updateProduct(id, productVO));		
	}
	
	@DeleteMapping("/products/{id}")
	public Mono<ResponseEntity<Boolean>> deleteProduct(@PathVariable String id){
		return this.productService.deleteProduct(id).flatMap(success -> {
			return Mono.just(ResponseEntity.status(HttpStatus.OK).body(null));	
		});
		
	}

}
