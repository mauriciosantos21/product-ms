package com.desafio.microservice.v1.service;

import java.util.List;
import java.util.Optional;

import com.desafio.microservice.v1.entity.Product;
import com.desafio.microservice.v1.model.ProductVO;

import reactor.core.publisher.Mono;

public interface IProductService {
	
	Mono<List<Product>> getAllProducts();
	Mono<Optional<Product>> getProductById(String id);
	Mono<List<Product>> getAllProductsByParams(Double minPrice, Double maxPrice, String q);
	Mono<Product> addProduct(ProductVO productVO);
	Mono<Product> updateProduct(String id, ProductVO productVO);
	Mono<Boolean> deleteProduct(String id);

}
