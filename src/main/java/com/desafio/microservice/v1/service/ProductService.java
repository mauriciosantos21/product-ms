package com.desafio.microservice.v1.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.desafio.microservice.exception.FindException;
import com.desafio.microservice.exception.RegisterException;
import com.desafio.microservice.v1.entity.Product;
import com.desafio.microservice.v1.model.ProductVO;
import com.desafio.microservice.v1.repository.ProductRepository;

import reactor.core.publisher.Mono;

@Service
public class ProductService implements IProductService {

	@Autowired
	private ProductRepository repository;

	private static final String MSG_PRODUCT_NOT_FOUND = "Produto não encontrado na base";
	private static final String MSG_REGISTER_INVALID_VALUES = "Cadastro não aceita valores em branco ou vazio";
	private static final String MSG_UPDATE_INVALID_VALUES = "A atualização de produtos não aceita valores em branco ou vazio";
	private static final String MSG_PRICE_NEGATIVE = "Campo 'PRICE' não aceita valores negativos";

	@Override
	public Mono<List<Product>> getAllProducts() {
		return Mono.just(repository.findByAll());
	}

	@Override
	public Mono<Optional<Product>> getProductById(String id) {
		Optional<Product> productBase = repository.findById(id);
		if (!productBase.isEmpty()) {
			return Mono.just(repository.findById(id));
		} else {
			return Mono.error(new FindException(MSG_PRODUCT_NOT_FOUND, HttpStatus.NOT_FOUND));
		}
	}

	@Override
	public Mono<List<Product>> getAllProductsByParams(Double minPrice, Double maxPrice, String q) {
		if(Objects.nonNull(maxPrice) && Objects.nonNull(minPrice) && StringUtils.isNotBlank(q)) {
			return Mono.just( repository.findByNameOrDescriptionAndPriceBetween(q, q, minPrice, maxPrice));			
		}else if(Objects.nonNull(maxPrice) && Objects.nonNull(minPrice) && StringUtils.isEmpty(q)) {
			return Mono.just(repository.findByPriceBetween(minPrice, maxPrice));
		}else if(Objects.nonNull(minPrice) && StringUtils.isNotBlank(q) && Objects.isNull(maxPrice) ) {
			return Mono.just(repository.findByNameOrDescriptionAndPriceGreaterThanEqual(q, q, minPrice));
		}else if(Objects.nonNull(maxPrice) && StringUtils.isNotBlank(q) && Objects.isNull(minPrice)) {
			return Mono.just(repository.findByNameOrDescriptionAndPriceLessThanEqual(q, q, maxPrice));
		}else if(Objects.nonNull(minPrice) && StringUtils.isEmpty(q) && Objects.isNull(maxPrice) ) {
			return Mono.just(repository.findByPriceGreaterThanEqual(minPrice));
		}else if(Objects.nonNull(maxPrice) && StringUtils.isEmpty(q) && Objects.isNull(minPrice)) {
			return Mono.just(repository.findByPriceLessThanEqual(maxPrice));
		}else if(Objects.isNull(maxPrice) && Objects.isNull(minPrice) && StringUtils.isNotBlank(q)){
			return Mono.just(repository.findByNameOrDescription(q, q));
		}else {
			return Mono.just(new ArrayList<>());
		}
	}

	@Override
	public Mono<Product> addProduct(ProductVO productVO) {
		if (StringUtils.isNotBlank(productVO.getName()) && StringUtils.isNotBlank(productVO.getDescription())
				&& null != productVO.getPrice()) {
			if (productVO.getPrice() >= 0) {
				return Mono.just( repository
						.save(new Product(productVO.getName(), productVO.getDescription(), productVO.getPrice())));
			} else {
				return Mono.error( new RegisterException(MSG_PRICE_NEGATIVE, HttpStatus.BAD_REQUEST));
			}
		} else {
			return Mono.error( new RegisterException(MSG_REGISTER_INVALID_VALUES, HttpStatus.BAD_REQUEST));
		}
	}

	@Override
	public Mono<Product> updateProduct(String id, ProductVO productVO) {
		Optional<Product> productBase = repository.findById(id);
		if (productBase.isEmpty()) {
			return Mono.error( new RegisterException(MSG_PRODUCT_NOT_FOUND, HttpStatus.NOT_FOUND));
		}
		if (StringUtils.isNotBlank(productVO.getName()) && StringUtils.isNotBlank(productVO.getDescription())
				&& null != productVO.getPrice()) {
			if (productVO.getPrice() >= 0) {
				return Mono.just(repository
						.save(new Product(id, productVO.getName(), productVO.getDescription(), productVO.getPrice())));
			} else {
				return Mono.error( new RegisterException(MSG_PRICE_NEGATIVE, HttpStatus.BAD_REQUEST));
			}
		} else {
			return Mono.error( new RegisterException(MSG_UPDATE_INVALID_VALUES, HttpStatus.BAD_REQUEST));
		}

	}

	@Override
	public Mono<Boolean> deleteProduct(String id) {
		Optional<Product> productBase = repository.findById(id);
		if (!productBase.isEmpty()) {
			repository.deleteById(id);
			return Mono.just(true);
		} else {
			return Mono.error( new FindException(MSG_PRODUCT_NOT_FOUND, HttpStatus.NOT_FOUND));
		}
	}

}
