package com.desafio.microservice.v1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.desafio.microservice.v1.entity.Product;

public interface ProductRepository extends CrudRepository<Product, String> {

	@Query(value = "SELECT * FROM PRODUCT p WHERE (p.name = ?1 or p.description =?2) and (p.price between ?3 and ?4)", nativeQuery = true)
	List<Product> findByNameOrDescriptionAndPriceBetween(String name, String description, Double minPrice,
			Double maxPrice);
	@Query(value = "SELECT * FROM PRODUCT p WHERE (p.name = ?1 or p.description =?2) and (p.price >= ?3)", nativeQuery = true)
	List<Product> findByNameOrDescriptionAndPriceGreaterThanEqual(String name, String description, Double minPrice);
	@Query(value = "SELECT * FROM PRODUCT p WHERE (p.name = ?1 or p.description =?2) and (p.price <= ?3)", nativeQuery = true)
	List<Product> findByNameOrDescriptionAndPriceLessThanEqual(String name, String description, Double maxPrice);

	List<Product> findByPriceGreaterThanEqual(Double minPrice);

	List<Product> findByPriceLessThanEqual(Double maxPrice);

	List<Product> findByNameOrDescription(String name, String description);

	List<Product> findByPriceBetween(Double minPrice, Double maxPrice);
	
	@Query(value = "SELECT * FROM PRODUCT", nativeQuery = true)
	List<Product> findByAll();

}
